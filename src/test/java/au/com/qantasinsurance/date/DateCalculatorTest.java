package au.com.qantasinsurance.date;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DateCalculatorTest {

    @ParameterizedTest
    @MethodSource("dateDiffer")
    void testDateCalculator(MyDate fromDay, MyDate toDay, int expectedValue) {
        CountDate resultCalculated = new DateCalculator();
        assertEquals(expectedValue, resultCalculated.dateCounted(fromDay, toDay), "Calculator is broken, test failed");
    }

    static Stream<Arguments> dateDiffer() {
        return Stream.of(
                Arguments.of(new MyDate(1983, 6, 2), new MyDate(1983, 6, 22), 19),
                Arguments.of(new MyDate(1984, 7, 4), new MyDate(1984, 12, 25), 173),
                Arguments.of(new MyDate(989, 1, 3), new MyDate(1983, 8, 3), 363261)
        );
    }


}




