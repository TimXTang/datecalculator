package au.com.qantasinsurance.date;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MyDateTest {

    @ParameterizedTest  //hard to test because MyDate with three parameter is hard to test using assertEquals.
    @MethodSource("testSubjects1")
    void testSetter(MyDate actualDate, MyDate expectedDate) {
        int actualYear = actualDate.getYear();
        int expectedYear = expectedDate.getYear();
        int actualMonth = actualDate.getMonth();
        int expectedMonth = expectedDate.getMonth();
        int actualDay = actualDate.getDay();
        int expectedDay = expectedDate.getDay();
        assertEquals(expectedYear, actualYear, "Test failed on years");
        assertEquals(expectedMonth, actualMonth, "test failed on months");
        assertEquals(expectedDay, actualDay, "test failed on days");

    }

    static Stream<Arguments> testSubjects1() {
        return Stream.of(
                Arguments.of(MyDate.of(1984, 3, 3), new MyDate(1984, 3, 3)),
                Arguments.of(MyDate.of(1888, 4, 4), new MyDate(1888, 4, 4))
        );
    }


    @ParameterizedTest
    @MethodSource("testSubjects2")
    void testGetter(int testYear, int testMonth, int testDay, String testDate) {
        MyDate md = MyDate.of(testYear, testMonth, testDay);
        int actualYear = md.getYear();
        int actualMonth = md.getMonth();
        int actualDay = md.getDay();
        String actualDate = md.getDateInString();
        assertEquals(actualYear, testYear, "getYear() doesn't work");
        assertEquals(actualMonth, testMonth, "getMonth() doesn't work");
        assertEquals(actualDay, testDay, "getDay() doesn't work");
        assertEquals(actualDate, testDate, "getDateInString() doesn't work");
    }

    static Stream<Arguments> testSubjects2() {
        return Stream.of(
                Arguments.of(1984, 12, 03, "1984-12-3"),
                Arguments.of(1873, 03, 05, "1873-3-5")
        );
    }


    @ParameterizedTest
    @MethodSource("testSubjects3")
    void testDaysParameter(MyDate testedDate, int expectedDaysInThisMonth, int expectedDaysInThisYear) {
        int actualDaysInTotal = testedDate.daysInThisMonth();
        assertEquals(actualDaysInTotal,expectedDaysInThisMonth,"daysInThisMonth() doesn't work");
        assertEquals(testedDate.daysInThisYear(),expectedDaysInThisYear, "daysInThisYear(0 doesn't work");
    }

    static Stream<Arguments> testSubjects3() {
        return Stream.of(
                Arguments.of(MyDate.of(1999, 9, 03), 30, 365),
                Arguments.of(MyDate.of(2000, 2, 03), 29, 366),
                Arguments.of(MyDate.of(1990, 2, 03), 28, 365)
        );
    }
}
