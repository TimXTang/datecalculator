package au.com.qantasinsurance.date;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ValidateDatesUsingStringMatchTest {
    private String dateFormat = "[12]\\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\\d|3[01])";
    private ValidateDatesUsingStringMatch mvalidator;

    @ParameterizedTest
    @MethodSource("testSubjects")
    void validatedMyDates(String testedInputs, MyDate expectedDates) {
        mvalidator = Mockito.spy(ValidateDatesUsingStringMatch.class);
        mvalidator.setDatesFormat(dateFormat);
        MyDate validatedDates = mvalidator.validatedMyDates(testedInputs);// it didn't work if the tested class is using configuration /yml/properties.
        if (validatedDates != null) {
            assertEquals(validatedDates.getDateInString(), expectedDates.getDateInString(), "Validator doesn't work");
            System.out.println("validated output is : "+ validatedDates.getDateInString() + " and the expected Dates is : " + expectedDates.getDateInString());
        } else {
            assertEquals(validatedDates, expectedDates, "Validator doesn't work");
        }

    }

    static Stream<Arguments> testSubjects() {
        return Stream.of(
                Arguments.of("1983-12-13", MyDate.of(1983, 12, 13)),
                Arguments.of("2000-02-29", MyDate.of(2000, 02, 29)),
                Arguments.of("1990-02-29", null),
                Arguments.of("1923-003-13", null)
        );
    }

}