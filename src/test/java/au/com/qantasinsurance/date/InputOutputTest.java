package au.com.qantasinsurance.date;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;

import java.io.PrintStream;
import java.util.Scanner;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class InputOutputTest {

    private InputOutput inputOutput;
    private PrintStream printStream;
    private Scanner scanner;
    private Scanner createScanner(String string) {
        return new Scanner(string);
    }


    @ParameterizedTest
    @MethodSource("testSubjectsInput")
    void testInput(String userInput, String systemReadInput) {
        printStream = Mockito.mock(PrintStream.class);
        scanner = createScanner(userInput);
        inputOutput = new InputOutput();
        inputOutput.setPs(printStream);
        inputOutput.setScan(scanner);
        assertEquals(systemReadInput, inputOutput.getFirstInput(), "Input test failed");

    }

    static Stream<Arguments> testSubjectsInput() {
        return Stream.of(
                Arguments.of("1984-03-03", "1984-03-03"),
                Arguments.of("2003-03-23", "2003-03-23"),
                Arguments.of("2000-02-30", "2000-02-30")
        );
    }

    @Test
    void testOutput() {
        MyDate dayFrom = MyDate.of(1990,9,9);
        MyDate dayTo = MyDate.of(2000,02,29);
        CountDate mCountDate = Mockito.mock(CountDate.class);
        inputOutput = new InputOutput();
        inputOutput.setCountDate(mCountDate);
        inputOutput.getOutput(dayFrom,dayTo);
        Mockito.verify(mCountDate).dateCounted(dayFrom,dayTo); //verify the mocked CountDate.dateCounted is used
    }

}




//
/*
    @Test
    public void test() throws Exception {
        printStream = Mockito.mock(PrintStream.class);
        scanner = createScanner("123123");
        inputOutput = new InputOutput();
        inputOutput.setPs(printStream);
        inputOutput.setScan(scanner);
        inputOutput.setRequestFirstInputMessage("first message");
        assertEquals(inputOutput.getFirstInput(), "1984-03-23", "test failed");
        Mockito.verify(printStream).print("first message");
    }
*/
/*    @Test
    public void testInput() {
        String expectedInput = "1111-01-01";
        InputStream in = new ByteArrayInputStream(expectedInput.getBytes());
        System.setIn(in);
        InputOutput inputOne = new InputOutput();
        assertEquals(expectedInput, inputOne.getFirstInput(), "failed");
    }*/