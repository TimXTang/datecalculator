package au.com.qantasinsurance.date;


//this can be a bean
//setYear(),setMonth,setDay . use another dateEntry to setYear, set Month, setDay;
//this is a typical object, with every parameter to MyDate and can be recalled by others

public class MyDate {

    private int year;
    private int month;
    private int day;

    public MyDate(int year, int month, int day) {
        this.year = year;
        this.month = month;
        this.day = day;
    } // this is the constructor

    public static MyDate of(int year, int month, int day) {
        return new MyDate(year, month, day);
    } //setting a new MyDate;   this can be replaced by MyDate(int year, int month, int day) above


    public int getYear() {
        return year;
    }

    public int getMonth() {
        return month;
    }

    public int getDay() {
        return day;
    }

    public String getDateInString(){
        return year + "-" + month + "-" + day;
    }



    public boolean leapYearOrNot() {
        if (year % 100 == 0 && year % 400 == 0) {
            return true;
        } else if (year % 4 == 0 && year % 100 != 0) {
            return true;
        } else {
            return false;
        }
    } // this is to determine whether the year is a leap year;

    public int daysInThisYear() {

        if (leapYearOrNot()) {

            return 366;
        } else {
            return 365;
        }
    } // this is used to indicate how many days are in that year based on whether its leap year or not.

    public int daysInThisMonth() {
        if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
            return 31;
        } else if (month == 4 || month == 6 || month == 9 || month == 11) {
            return 30;
        } else if (leapYearOrNot()) {
            return 29;
        } else {
            return 28;
        }
    } // this is to days in different months

    public static boolean sortOf(String string) throws Exception {
        if (string.matches("[001-2222][-][0-12][0-31]")) {
            System.out.print("validated");
            return true;

        } else {
            throw new Exception("wrong format");
        }
    }


}
