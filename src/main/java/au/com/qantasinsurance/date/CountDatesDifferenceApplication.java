package au.com.qantasinsurance.date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.PropertySource;


@SpringBootApplication
@EnableConfigurationProperties
@PropertySource("classpath:/application.yml")
public class CountDatesDifferenceApplication implements CommandLineRunner {
    @Autowired
    InputOutput inputOutput;
    @Autowired
    DatesFormatValidator datesFormatValidator;

    @Override
    public void run(String... args) throws Exception {
        MyDate dayFrom = datesFormatValidator.validatedMyDates(inputOutput.getFirstInput());    //calling the input and validates it through validator
        while(dayFrom == null){
            System.out.print("Again, " );
            dayFrom = datesFormatValidator.validatedMyDates(inputOutput.getFirstInput());
        }
        MyDate dayTo = datesFormatValidator.validatedMyDates(inputOutput.getSecondInput());
        while(dayTo == null){
            System.out.print("Again, " );
            dayTo = datesFormatValidator.validatedMyDates(inputOutput.getSecondInput());
        }
        int outputResult = inputOutput.getOutput(dayFrom,dayTo);                   //injecting the validated inputs to the calculator and get results.
        System.out.println("The number of full day elapsed between your two dates is: " + outputResult);
    }

    public static void main(String[] args) {

        SpringApplication app = new SpringApplication(CountDatesDifferenceApplication.class);
        app.run(args);
    }


}
    /*
    @Autowired
    private DatesFormatValidator datesFormatValidator;
    @Autowired
    private CountDate countDate;
    @Value("${requestFirstInputMessage}")
    private String requestFirstInputMessage;
    @Value("${requestSecondInputMessage}")
    private String requestSecondInputMessage;
*/

/*
        Scanner scan = new Scanner(System.in);
        System.out.print(requestFirstInputMessage);
        String dates = scan.next();
        public String getInput(){return dates;}
        MyDate dayFrom = datesFormatValidator.validatedMyDates(dates);
        while (dayFrom == null) {
            System.out.print("Again, "+requestFirstInputMessage);
            dates = scan.next();
            dayFrom = datesFormatValidator.validatedMyDates(dates);
        }

        System.out.print(requestSecondInputMessage);
        dates = scan.next();
        MyDate dayTo = datesFormatValidator.validatedMyDates(dates);
        while (dayTo == null) {
            System.out.print("Again, "+requestSecondInputMessage);
            dates = scan.next();
            dayTo = datesFormatValidator.validatedMyDates(dates);
        }
        int calculatedResult = countDate.dateCounted(dayFrom, dayTo);
        System.out.println("The number of full day elapsed between your two dates is: " + calculatedResult);
        */


