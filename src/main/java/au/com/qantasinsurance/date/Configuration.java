package au.com.qantasinsurance.date;

import org.springframework.context.annotation.Bean;

import java.io.PrintStream;

@org.springframework.context.annotation.Configuration
public class Configuration {

    @Bean
    public PrintStream printStream() {
        return System.out;
    }
}
