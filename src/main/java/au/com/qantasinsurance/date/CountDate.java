package au.com.qantasinsurance.date;

/*Calculate the amount of full days elapsed between two dates,
for example, 18/01/2009 to 19/01/2009 should return zero.
dayFrom needs to be the earlier date with the format of yyyy,mm,dd
dayTo needs to be the later date with the format of yyyy,mm,dd*/

public interface CountDate {
    int dateCounted(MyDate dayFrom, MyDate dayTo);
}
