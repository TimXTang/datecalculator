package au.com.qantasinsurance.date;

import org.springframework.stereotype.Component;

@Component

public class DateCalculator implements CountDate {
    //assume the dateTo is always in the future of dateFrom
//@Value("#{yearInTheFirstDate")
//private Integer yearInTheFirstDate;

    public int dateCounted(MyDate dateFrom, MyDate dateTo) {
        int result;
        int dayDifference = dateTo.getDay() - dateFrom.getDay();
        int monthDifference = 0;
        int yearDifference = 0;
        MyDate dateModifier = MyDate.of(dateFrom.getYear(), dateFrom.getMonth(), dateFrom.getDay());
        if (dateFrom.getYear() == dateTo.getYear()) {
            for (int i = 0; i < dateTo.getMonth() - dateFrom.getMonth(); i++) {
                monthDifference = monthDifference + dateModifier.daysInThisMonth();
                dateModifier = MyDate.of(dateModifier.getYear(), (dateModifier.getMonth() + 1), dateModifier.getDay());
            }

        } else {
            for (int i = 0; i < 12 - dateFrom.getMonth(); i++) {
                monthDifference = monthDifference + dateModifier.daysInThisMonth();
                dateModifier = MyDate.of(dateModifier.getYear(), (dateModifier.getMonth() + 1), dateModifier.getDay());
            }
            dateModifier = MyDate.of(dateModifier.getYear(), 1, 1);
            for (int j = 0; j < dateTo.getMonth(); j++) {
                monthDifference = monthDifference + dateModifier.daysInThisMonth();
                dateModifier = MyDate.of(dateModifier.getYear(), (dateModifier.getMonth() + 1), dateModifier.getDay());
            }
            for (int k = 1; k < dateTo.getYear() - dateFrom.getYear(); k++) {
                yearDifference = yearDifference + dateModifier.daysInThisYear();
                dateModifier = MyDate.of(dateModifier.getYear() + 1, (dateModifier.getMonth()), dateModifier.getDay());
            }
        }
        result = yearDifference + monthDifference + dayDifference;
        return --result;//return the value of result - 1 to int showDateCounted;
    }
}


        /*
        for (int i = 0; i < 12 - dateFrom.getMonth(); i++) {
            monthDifference = monthDifference + dateModifier.daysInThisMonth();
            dateModifier = MyDate.of(dateModifier.getYear(), (dateModifier.getMonth() + 1), dateModifier.getDay());
        }// calculate the dates from Month Difference. Loop
        for (int j = 0; j < dateTo.getYear() - dateFrom.getYear(); j++) {
            yearDifference = yearDifference + dateModifier.daysInThisYear();
            dateModifier = MyDate.of(dateModifier.getYear() + 1, (dateModifier.getMonth()), dateModifier.getDay());
        }


        if (dateTo.getMonth() >= dateFrom.getMonth()) {
            for (int i = 0; i < dateTo.getMonth() - dateFrom.getMonth(); i++) {
                monthDifference = monthDifference + dateModifier.daysInThisMonth();
                dateModifier = MyDate.of(dateModifier.getYear(), (dateModifier.getMonth() + 1), dateModifier.getDay());
            }// calculate the dates from Month Difference. Loop
            for (int j = 0; j < dateTo.getYear() - dateFrom.getYear(); j++) {
                yearDifference = yearDifference + dateModifier.daysInThisYear();
                dateModifier = MyDate.of(dateModifier.getYear() + 1, (dateModifier.getMonth()), dateModifier.getDay());
            }
        } else if (dateTo.getMonth() < dateFrom.getMonth()) {
            for (int i = 0; i < 12 - dateFrom.getMonth(); i++) {
                monthDifference = monthDifference + dateModifier.daysInThisMonth();
                dateModifier = MyDate.of(dateModifier.getYear(), (dateModifier.getMonth() + 1), dateModifier.getDay());
            }// calculate the dates from Month Difference. Loop
            dateModifier = MyDate.of(dateModifier.getYear(), 1, 1);
            for (int i = 0; i < dateTo.getMonth(); i++) {
                monthDifference = monthDifference + dateModifier.daysInThisMonth();
                dateModifier = MyDate.of(dateModifier.getYear(), (dateModifier.getMonth() + 1), dateModifier.getDay());
            }
            for (int j = 0; j < dateTo.getYear() - dateFrom.getYear(); j++) {
                yearDifference = yearDifference + dateModifier.daysInThisYear();
                dateModifier = MyDate.of(dateModifier.getYear() + 1, (dateModifier.getMonth()), dateModifier.getDay());
            }
        }


    }
}
*/