package au.com.qantasinsurance.date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.PrintStream;
import java.util.Scanner;

@Component
public class InputOutput {

    @Autowired
    private CountDate countDate;

    @Value("${requestFirstInputMessage}")
    private String requestFirstInputMessage;

    @Value("${requestSecondInputMessage}")
    private String requestSecondInputMessage;

    private String dates;
    public Scanner scan = new Scanner(System.in);

    @Autowired
    private PrintStream ps;

    private int calculatedResult;

    public String getFirstInput() {
        ps.print(requestFirstInputMessage);
        dates = scan.next();
        return dates;
    }

    public String getSecondInput() {
        ps.print(requestSecondInputMessage);
        dates = scan.next();
        return dates;
    }

    public int getOutput(MyDate dayFrom, MyDate dayTo) {
        calculatedResult = countDate.dateCounted(dayFrom, dayTo);
        return calculatedResult;
    }

    void setPs(PrintStream ps) {
        this.ps = ps;
    }

    void setScan(Scanner scan) {
        this.scan = scan;
    }
    public void  setCountDate(CountDate countDate){
         this.countDate = countDate;
    }
}