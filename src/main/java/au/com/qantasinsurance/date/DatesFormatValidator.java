package au.com.qantasinsurance.date;

/*
this interface is used to validate whether a string meets the requirement of yyyy-mm-dd
the amounts of digits are fixed means Feb. is 02 not 2
then the string with the format will be converted to MyDate with the format  of MyDate.of(yyyy,mm,dd)
 */

public interface DatesFormatValidator {
    MyDate validatedMyDates(String string);
}
