package au.com.qantasinsurance.date;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Component
@Configuration
public class ValidateDatesUsingStringMatch implements DatesFormatValidator {

    @Value("${formatCorrectWarning}")
    private String formatCorrectWarning;
    @Value("${datesFormat}")
    private String datesFormat;
    @Value("${formatIncorrectWarning}")
    private String formatIncorrectWarning;


    @Override
    public MyDate validatedMyDates(String input) {

        String regex = datesFormat;
        if (input.matches(regex)) {
            int year = Integer.parseInt(input.substring(0, input.indexOf('-')));
            int month = Integer.parseInt(input.substring(5, 7));
            int day = Integer.parseInt(input.substring(8, 10));
            MyDate myDate = MyDate.of(year, month, day);
            if (myDate.getDay() <= myDate.daysInThisMonth()) {
                System.out.println(formatCorrectWarning);
                return new MyDate(year, month, day);
            } else {
                System.out.println("please check the amount of days in the corresponding month");
                return null;
            }
        } else {
            System.out.println(formatIncorrectWarning);
            return null;
        }

    }
    void setDatesFormat(String datesFormat){
        this.datesFormat = datesFormat;
    }
}



 /*
        [12]: 1 or 2 at the first digit
        \\d : equal to [0-9]
        {3}: together with \\d means the following three digits need to match 0-9
        -: matches exactly a -
        0[1-9]: first digit is 0 and second digit is from 1 to 9 means Jan to Sep
        | : or
        1[0-2]: first digit is 1 and second digit is from 0 to 2 means Oct to Dec
        (0[1-9]|[12]\d|3[01]): 01-09 or 10-29 or 30-31
         */
